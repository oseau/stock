Initialise
[setup git](https://confluence.atlassian.com/display/BITBUCKET/Set+up+Git)
[setup Python Development Environment on Ubuntu](http://webees.me/setting-up-python-development-environment-on-ubuntu-12-04-lts/)
```
git clone https://oseau@bitbucket.org/oseau/stock.git
mkvirtualenv stock
pip install -r requirements.txt

```
