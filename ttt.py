# -*- coding: utf-8 -*-
import eventlet
from eventlet.green import urllib2
import re
import csv
import os
from random import randint
import time
import shutil

class Stock(object):
    def __init__(self, stock_code, stock_name, suffix):
        self.__stock_code = stock_code
        self.__stock_name = stock_name
        self.__suffix = suffix
        self.__csv = []

    @property
    def stock_code(self):
        return self.__stock_code

    @property
    def stock_name(self):
        return self.__stock_name

    @property
    def suffix(self):
        return self.__suffix

    def __str__(self):
        return '(%s): %s' % (self.__stock_code, self.__stock_name)
    __repr__ = __str__

    def url(self):
        return 'http://table.finance.yahoo.com/table.csv?s=%s.%s'%(self.__stock_code,self.__suffix)

    def read_from_csv(self):
        csv.register_dialect('stock_dialect', delimiter=',', quoting=csv.QUOTE_NONE)
        try:
            with open(os.path.join(os.path.dirname(__file__),'csv',self.__stock_code+'.csv'), 'rb') as f:
                reader = csv.reader(f, 'stock_dialect')
                for row in reader:
                    if len(row[0])==10:
                        self.__csv.append(row)
        except:
            self.__csv = None

    def drops_from_today(self):
        self.read_from_csv()
        i=0
        while True and self.__csv:
            if self.__csv[i][4]>=self.__csv[i+1][4]:
                break
            else:
                i=i+1
        return i+1

def stocks():
    """return a list of dict{code: Stock.object}"""
    f = open('stock_code.txt', 'rU')
    a = f.read().split('\n')
    f.close()
    b0=re.findall("\\s*(\\D+)\\(([0,3,6]\\d+)\\)", a[0])
    b1=re.findall("\\s*(\\D+)\\(([0,3,6]\\d+)\\)", a[1])
    stock_market0 = [Stock(x[1],x[0],'ss') for x in b0]+[Stock(x[1],x[0],'sz') for x in b1]
    stock_market = dict((x.stock_code,x) for x in stock_market0)
    return stock_market

def fetch(stock):
    script_dir = os.path.dirname(__file__)
    rel_path = "csv/%s.csv"%stock.stock_code
    abs_file_path = os.path.join(script_dir, rel_path)
    if not os.path.isfile(abs_file_path):
        time.sleep(randint(1,3))
        print('start fetching %s'%stock.url())
        try:
            return urllib2.urlopen(stock.url()).read(), stock.stock_code
        except:
            print('no result %s'%stock.url())
            return False, False
    else:
        return False, False

def download_csv():
    pool = eventlet.GreenPool(3)
    urls = stocks().values()
    if not os.path.exists('csv'):
        os.makedirs('csv')
    for body, code in pool.imap(fetch, urls):
        if body and code:
            print("got body", code)
            script_dir = os.path.dirname(__file__)
            rel_path = "csv/%s.csv"%code
            abs_file_path = os.path.join(script_dir, rel_path)
            with open(abs_file_path, 'w+b') as f:
                f.write(body)



    # for file in os.listdir(os.path.join(os.path.dirname(__file__),'csv')):
    #     if file.endswith(".csv"):
    #         print os.path.join(os.path.dirname(__file__),'csv',file)


def main():
    t = time.time()
    if os.path.exists('csv'):
        shutil.rmtree('csv')
    download_csv()
    a = [x for x in stocks().values() if x.drops_from_today() > 5]
    print a
    print len(a)
    print a[1]
    print a[1].drops_from_today()
    print(time.time()-t)

if __name__ == '__main__':
    main()